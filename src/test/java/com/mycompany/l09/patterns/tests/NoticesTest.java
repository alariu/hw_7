package com.mycompany.l09.patterns.tests;

import com.mycompany.l09.patterns.business_logic.Intra;
import com.mycompany.l09.patterns.page_objects.UserMainPage;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class NoticesTest extends TestBase {

    private Intra intra;

    @BeforeMethod
    public void initData() {
        intra = app.getIntra();
    }

    static Loader loader = new Loader();
    public static final String USERNAME = loader.getLogin();
    public static final String USERPASSWORD = loader.getPassword();

    @Test
    public void openNotices() {
        UserMainPage userMainPage = new UserMainPage(app.getWebDriver());

        intra.login(USERNAME, USERPASSWORD);
        userMainPage.openNotices()
                .assertNoticesIsOpened();
    }
}
