package com.mycompany.l09.patterns.business_logic;


import com.mycompany.l09.patterns.domain.Group;
import com.mycompany.l09.patterns.page_objects.GroupDetailsPage;
import com.mycompany.l09.patterns.page_objects.SearchGroupsPage;

public class Groups {

    private final SearchGroupsPage searchGroupsPage;

    Groups(SearchGroupsPage searchGroupsPage) {
        this.searchGroupsPage = searchGroupsPage;
    }

    public GroupDetails createGroup(Group group) {
        GroupDetailsPage page = searchGroupsPage.clickAddGroup()
                .enterName(group.getName())
                .enterDescription(group.getDescription())
                .setOpen(group.isOpen())
                .save();
        return new GroupDetails(page);
    }

    public GroupDetails searchGroup(String groupName) {
        GroupDetailsPage page = searchGroupsPage.enterGroupSearch(groupName)
                .clickGroupInSearchResults(groupName);

        return new GroupDetails(page);
    }

}
