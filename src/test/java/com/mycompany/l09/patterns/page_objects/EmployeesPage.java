package com.mycompany.l09.patterns.page_objects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;

import static org.openqa.selenium.support.ui.ExpectedConditions.elementToBeClickable;

public class EmployeesPage extends BasePage {

    @FindBy(xpath = "//p[contains(text(),'Все пользователи')]")
    private WebElement filterButton;

    @FindBy(css = ".SearchInput-Input")
    private WebElement searchField;

    @FindBy(xpath = "//a[@href='/user/86555257-1454-4717-925d-9b79300cda30']")
    private WebElement user1;



    public EmployeesPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver,this);
    }

    public EmployeesPage assertEmployeesIsOpened () {
        wait.until(ExpectedConditions.visibilityOf(filterButton));
        Assert.assertTrue(filterButton.isDisplayed());
        return new EmployeesPage(driver);
    }

    public Employee1Page openEmployee1Page () {
        searchField.clear();
        searchField.sendKeys("Ишков");
        wait.until(elementToBeClickable(user1)).click();
        return new Employee1Page(driver);
    }
}
