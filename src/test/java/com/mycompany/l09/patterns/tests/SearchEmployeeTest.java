package com.mycompany.l09.patterns.tests;

import com.mycompany.l09.patterns.business_logic.Intra;
import com.mycompany.l09.patterns.page_objects.Employee1Page;
import com.mycompany.l09.patterns.page_objects.EmployeesPage;
import com.mycompany.l09.patterns.page_objects.UserMainPage;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class SearchEmployeeTest extends TestBase {

    private Intra intra;

    @BeforeMethod
    public void initData() {
        intra = app.getIntra();
    }

    static Loader loader = new Loader();
    public static final String USERNAME = loader.getLogin();
    public static final String USERPASSWORD = loader.getPassword();

    @Test
    public void openEmployee1() {
        UserMainPage userMainPage = new UserMainPage(app.getWebDriver());
        EmployeesPage employeesPage = new EmployeesPage(app.getWebDriver());

        intra.login(USERNAME, USERPASSWORD);
        userMainPage.openEmployees()
                .assertEmployeesIsOpened();
        employeesPage.openEmployee1Page()
                .assertEmployee1IsOpened();
    }


}
