package com.mycompany.l09.patterns.page_objects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;

public class Employee1Page extends BasePage{

    @FindBy(xpath = "//div[contains(text(),'Владислав Ишков')]")
    private WebElement user1Name;


    public Employee1Page(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver,this);
    }

    public Employee1Page assertEmployee1IsOpened () {
        wait.until(ExpectedConditions.visibilityOf(user1Name));
        Assert.assertTrue(user1Name.isDisplayed());
        return new Employee1Page(driver);
    }
}
