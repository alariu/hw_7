package com.mycompany.l09.patterns.tests;

import com.mycompany.l09.patterns.app.Application;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;

public class TestBase {

    Application app;

    @Parameters("browser")
    @BeforeMethod(alwaysRun = true)
    public void initData(@Optional("firefox") final String browser) {
        app = new Application(browser);
    }

    @AfterMethod
    public void stop() {
        app.stop();
    }

}
