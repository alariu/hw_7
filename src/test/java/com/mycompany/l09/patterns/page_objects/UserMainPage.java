package com.mycompany.l09.patterns.page_objects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static org.openqa.selenium.support.ui.ExpectedConditions.elementToBeClickable;

public class UserMainPage extends BasePage {

    @FindBy(xpath = "//a[@href='/groups']")
    private WebElement allGroupsLink;

    @FindBy(xpath = "//div[@title='Группы']")
    private WebElement groupsLink;

    @FindBy(xpath = "//div[@title='T-Store']")
    private WebElement tStoreIcon;

    @FindBy(xpath = "//a[@href='/shop']")
    private WebElement shopIcon;

    @FindBy(xpath = "//a[@href='/wikilist']")
    private WebElement companyNews;

    @FindBy(xpath = "//h3[text()='Внутренняя автоматизация: новости проекта']")
    private WebElement newsAboutInternalAutomation;

    @FindBy(xpath = "//div[@title='Приложения']")
    private WebElement appsIcon;

    @FindBy(xpath = "//a[@href='/notices']")
    private WebElement noticesIcon;

    @FindBy (xpath = "//div[@title='Основное']")
    private WebElement mainIcon;

    @FindBy(xpath = "//a[@href='/employees']")
    private WebElement employeesIcon;



    public UserMainPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public SearchGroupsPage clickGroups() {
        wait.until(elementToBeClickable(groupsLink)).click();
        wait.until(elementToBeClickable(allGroupsLink)).click();
        return new SearchGroupsPage(driver);
    }

    public TstorePage openStore() {
        wait.until(elementToBeClickable(tStoreIcon)).click();
        wait.until(elementToBeClickable(shopIcon)).click();
        sleep(20_000);
        return new TstorePage(driver);
    }

    public AnyNewsPage openNews() {
        wait.until(elementToBeClickable(companyNews)).click();
        wait.until(elementToBeClickable(newsAboutInternalAutomation)).click();
        return new AnyNewsPage(driver);
    }

    public NoticesPage openNotices() {
        wait.until(elementToBeClickable(appsIcon)).click();
        wait.until(elementToBeClickable(noticesIcon)).click();
        return new NoticesPage(driver);
    }

    public EmployeesPage openEmployees() {
        wait.until(elementToBeClickable(mainIcon)).click();
        wait.until(elementToBeClickable(employeesIcon)).click();
        return new EmployeesPage(driver);
    }
}
