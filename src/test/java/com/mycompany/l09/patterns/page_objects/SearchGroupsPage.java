package com.mycompany.l09.patterns.page_objects;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static org.openqa.selenium.support.ui.ExpectedConditions.elementToBeClickable;
import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOf;

public class SearchGroupsPage extends BasePage {

    @FindBy(xpath = "//input[@placeholder='Поиск групп']")
    private WebElement searchField;

    @FindBy(xpath = "//button[contains(text(),'Создать')]")
    private WebElement addGroupButton;

    SearchGroupsPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public SearchGroupsPage enterGroupSearch(String group) {
        wait.until(visibilityOf(searchField)).clear();
        searchField.sendKeys(group);
        searchField.sendKeys(Keys.ENTER);
        return this;
    }

    public AddGroupPage clickAddGroup(){
        wait.until(elementToBeClickable(addGroupButton)).click();
        return new AddGroupPage(driver);
    }

    public GroupDetailsPage clickGroupInSearchResults(String group) {
        wait.until(elementToBeClickable(By.linkText(group))).click();
        return new GroupDetailsPage(driver);
    }

}
