package com.mycompany.l09.patterns.page_objects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;

public class NoticesPage extends BasePage {

    @FindBy(css = ".InfoBlock-Info")
    private WebElement noticesPageTitel;

    NoticesPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver,this);
    }

    public NoticesPage assertNoticesIsOpened () {
        wait.until(ExpectedConditions.visibilityOf(noticesPageTitel));
        Assert.assertTrue(noticesPageTitel.isDisplayed());
        return new NoticesPage(driver);
    }
}
