package com.mycompany.l09.lombok;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//@Slf4j
public class Main {

    private static final Logger logger = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) throws NoSuchMethodException {
        new Group().getName();

        //logger.info("message");

        //new Exceptions().doSmth();
    }
}
